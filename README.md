# Gramatyka

## *Łączniki zdań*

### にしては / わりには

- wbrew oczekiwaniom, niespodziewanie...

#### Różnice:

にしては　nie może być używany z przymiotnikami; przyczyna zdziwienia musi być sprecyzowana

**高い**身長にしては、体重が軽い。

身長のわりには、体重が軽い。

このラーメンは安い値段のわりに美味しい。

このラーメンは値段のわりに美味しい。



### -た とたん / -た ばかり

- obie formy opisują, co się stało, zaraz po poprzedzającej czynności

#### Różnice:

とたん　czynność nastąpiła niezwłocznie i była niespodziewana

ばかりに　minęło trochę czasu zanim coś się stało, ale z perspektywy nie był to długi czas

日本に来たばかりのときは、大変だった。

パソコンを買ったとたん、壊れてしまいました。

### わりに / くせに

- efekt jest przeciwny do wyobrażeń

#### Różnice:

わりに możemy stosować do pozytywnych i negatywnych skutków、podczas gdy くせに tylko do negatywnych

この料理は安いわりに美味しい。



### まま / っぱなし

- opisują kontynuację stanu

#### Różnice:

- przymiotniki i rzeczowniki łączą się tylko z まま　(ずっと可愛いままです。)

- っぱなし może być również użyte do opisu czynności gdy jej efekt nie jest trwały

今日はお仕事で立ちっぱなしでした。lub ...たったままです。

エアコンがつけたままで外に出た。lub ...つけっぱなし

彼はずっと**歌いっぱなし**です。

### Warunki と / ば / たら / なら

#### Różnice:

- と jest zawsze prawdziwy

- ば / たら / なら są przypuszczające

- なら nie może być użyty do opisania skutku czynności warunkowej

  勉強したら、頭がよくなる。

  勉強すれば、頭がよくなる。

  勉強すると、頭がよくなる。

- Do opisu przyzwyczajeń i nawyków tylko たら lub と

  彼はご飯を食べたらは磨きする。

  彼はご飯を食べると磨きする。

- Wyrażenie czegoś niespodziewanego tylko たら lub と

  (w przeszłości) 

  学校に行ったら、今日は休みだった。

### はずだ / べきだ / わけ

- wyrażają powinność, oczekiwania

#### Różnice:

はず to co myślę że powinno się stać subiektywnie

べき to co jest dobrze, żeby zrobić obiektywnie

彼は昨日風邪だったから、今日こないはずでしょう。

でも今日は大事なテストだから来るべきです。

わけ wyraża tzw. 納得 czyli odpowiedź dlaczego coś się dzeje

あんなに食べたら太るわけです。



### だけ / のみ / きり

- tylko

#### Różnice:

きり tylko do liczb

のみ poza przymiotnikami

### たとえ / いくら / どんなに

- ile by nie robić, to skutek mizerny

どんなにチョコレートをたくさんあげても、振り向かない。

#### Różnice:

たとえ nie może być użyty w przeszłości

いくら stosujemy do częstotliwości pozostałe nie

いくら電話しても、つながらない。



